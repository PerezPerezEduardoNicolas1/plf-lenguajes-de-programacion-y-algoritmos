Programando ordenadores en los 80 y ahora ¿Qué ha cambiado?

```plantuml
@startmindmap
*[#red] Programando ordenadores \nen los 80 y ahora
**[#Orange] Sistema antiguo 
***[#lightgreen] Cualquier sistema que \nya no esta a la venta
***[#lightgreen] No ha tenido continuidad
***[#lightgreen] Ordenadores de los \naños 80s y 90s
****[#lightblue] Parecidos a los actuales \nen algunas caracteristicas
****[#lightblue] Potencia de los ordenadores \ninferior a las actuales
****[#lightblue] Velocidad de los CPUs se \nmedia en mhz 
****[#lightblue] Memoria muy inferior
****[#lightblue] Arquitecturas diferentes entre si
**[#Orange] Sistema actual
***[#lightgreen] Aquel que se puede comprar \nen una tienda
***[#lightgreen] Tienen continuidad
***[#lightgreen] Arquitecturas parecidas \nentre si
***[#lightgreen] Android
***[#lightgreen] IOS
***[#lightgreen] Ley de Wirth
****[#lightblue] El software se ralentiza más deprisa de lo que se acelera el hardware.
**[#Orange] Lenguajes \nusados
***[#lightgreen] Lenguajes de bajo nivel
****[#lightblue] Lenguaje ensamblador
*****[#FFBBCC] Programación complicada
*****[#FFBBCC] Aprovechamiento de recursos
*****[#FFBBCC] Control total de ejecución
*****[#FFBBCC] Programacion comoda
****[#lightblue] La maquina entiendo
****[#lightblue] Usados en ordenadores antiguos \npor la falta de recursos
****[#lightblue] Tienen gran eficiencia
***[#lightgreen] Lenguajes de alto nivel
****[#lightblue] Java 
*****[#FFBBCC] No se compila directamente a la maquina
*****[#FFBBCC] Maquina virtual
****[#lightblue] C
*****[#FFBBCC] Usa librerias 
****[#lightblue] Uniti
*****[#FFBBCC] Programación de videojuegos
****[#lightblue] Basado en como piensa \nel programador
****[#lightblue] Uso de interpretes
****[#lightblue] Uso de compiladores
****[#lightblue] Se pueden usar en ordenadores actuales debido\n a los recursos con lo que cuenta
****[#lightblue] Se pueden hacer software mas complejos
****[#lightblue] Poca eficiencia
****[#lightblue] Caso left-pad
@endmindmap
```

Hª de los algoritmos y de los lenguajes de programación

```plantuml
@startmindmap
*[#red] Hª de los algoritmos y de \nlos lenguajes de programación
**[#Orange] Algoritmo
***[#lightgreen] Es una lista bien definida ordenada \ny finita de operaciones que \npermite hallar la solución a un problema
***[#lightgreen] Ejemplos en la vida cotidiana
****[#lightblue] Manuales de usuario
****[#lightblue] Instrucciones que recibe un empleado
****[#lightblue] Recetas de cocina
***[#lightgreen] No puede ejecutarse hasta que se implemente
***[#lightgreen] Tiene un estado inicial
***[#lightgreen] Pueden hacer cosas mecanicas
***[#lightgreen] No pueden hacer procesos lógicos
***[#lightgreen] Tiene una entrada
***[#lightgreen] Importancia
***[#lightgreen] No se pueden resolver probelmas muy grandes
****[#lightblue] Radica en mostrar como llevar acabo procesos
****[#lightblue] Resuelve mecanicamente problemas 
****[#lightblue] Reciben una entrada y la transforman en una salida
***[#lightgreen] No da lugar a ambigüedades
***[#lightgreen] Los algoritmos estaban presentes en mesopotamia
***[#lightgreen] En el siglo 17 aparecen las primeras ayudas mecanicos
***[#lightgreen] A mediados del siglo 20 aparecen los ordenadores
**[#Orange] Leguajes de programación
***[#lightgreen] Son los instrumentos para comunicar los algoritmos a las computadores
***[#lightgreen] Existen diversos
***[#lightgreen] Paradigmas
****[#lightblue] imperativo
***[#lightgreen] Lenguajes pioneros
****[#lightblue] Fortran
****[#lightblue] Cobol
****[#lightblue] Basic
***[#lightgreen] Lenguajes actuales mas influyentes
****[#lightblue] Java
*****[#FFBBCC] Del año 1995 
*****[#FFBBCC] Orientado a objetos
****[#lightblue] C++
*****[#FFBBCC] Orientado a objetos
****[#lightblue] C
**[#Orange] Maquinas
***[#lightgreen] Ejecutan programas
***[#lightgreen] Trabajan con lenguajes de programación
***[#lightgreen] Las pianolas aparecen en el siglo 19
***[#lightgreen] Maquina analitica
****[#lightblue] Era completamente mecanica
**[#Orange] Informatica
***[#lightgreen] En la vida cotidiana
****[#lightblue] Manejo de procesadores de texto
****[#lightblue] Manejo de hojas de calculo
****[#lightblue] Aplicaciones de oficina
***[#lightgreen] Ingenieria en informatica
****[#lightblue] Diciplina que enseña a construir software
***[#lightgreen] Tiene una larga historia
@endmindmap
```

Tendencias actuales en los lenguajes de Programación y Sistemas Informáticos. La evolución de los lenguajes y paradigmas de programación
```plantuml
@startmindmap
*[#red] Tendencias actuales en los lenguajes de Programación y \nSistemas Informáticos. La evolución de los\n lenguajes y paradigmas de programación
**[#Orange] Paradigmas
***[#lightgreen] Es una manera para poder aproximarse \na la solución de un problema
***[#lightgreen] Tipos
****[#lightblue] Paradigma funcional
*****[#FFBBCC] Se apoya en las matematicas
*****[#FFBBCC] Esta basado en la recursividad
*****[#FFBBCC] Los lenguajes que usan este paradigma
******[#yellow] Ml
******[#yellow] Hope
******[#yellow] Haskel
****[#lightblue] Paradigma logico
*****[#FFBBCC] Basado en expresiones logicas
*****[#FFBBCC] Uso de predicados logicos
*****[#FFBBCC] Uso de recursividad
*****[#FFBBCC] Lenguajes que usan este paradigma
******[#yellow] Prolog
******[#yellow] Mercury
******[#yellow] CLP
**[#Orange] Tipos de programacion
***[#lightgreen] Programación estructurada
****[#lightblue] El diseño del software es modular
****[#lightblue] Tipos de estructuras de las memorias
*****[#FFBBCC] Abstractas
*****[#FFBBCC] De control
****[#lightblue] Lenguajes con esta estructura
*****[#FFBBCC] C
*****[#FFBBCC] Basic
*****[#FFBBCC] Pascal
*****[#FFBBCC] Fortran
****[#lightblue] Java
*****[#FFBBCC] Su programación es orientada a objetos
*****[#FFBBCC] Algoritmos de dialogo entre los objetos
***[#lightgreen] Programacion concurrente
****[#lightblue] Es una manera de concebir los problemas que surgen
****[#lightblue] Se deben programar ciertas politicas de sincronizacion
***[#lightgreen] Programacion distribuida
****[#lightblue] Surge desde que los ordenadores se pueden comunicar entre si 
***[#lightgreen] Programacion basada en componentes
****[#lightblue] Permite la reutilizacion 
***[#lightgreen] Programacion orientada a objetos
****[#lightblue] Implementa el esqueleto del programa incorporados 
****[#lightblue] Todos los aspectos son separados
***[#lightgreen] Programacion orientada a agentes de software y los sistemas multigente
****[#lightblue] Son aplicaciones infromaticas con capacidad de decidir \ncomo deben actuar para alcanzar sus objetivos
****[#lightblue] Sistemas multiagente  
@endmindmap
```